#ifndef COMPANY_H
#define COMPANY_H

#include <QString>
#include <QJsonObject>

class Company
{
public:
    Company(QString &&name, QString &&director, QString &&PIB, QString &&address, QString &&city, QString &&MB, QString &&contact = "", QString &&account = "");

    QString name() const;
    QString director() const;
    QString PIB() const;
    QString address() const;
    QString city() const;
    QString MB() const;
    QString contact() const;
    QString account() const;

    QJsonObject toJSON() const;

private:
    QString mName;
    QString mDirector;
    QString mPIB;
    QString mAddress;
    QString mCity;
    QString mMB;
    QString mContact;
    QString mAccount;
};

#endif // COMPANY_H
