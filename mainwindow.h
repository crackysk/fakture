#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <QPushButton>
#include <QVector>
#include <QProcess>
#include <QDebug>
#include <QDir>
#include <QTemporaryFile>
#include <QDialog>
#include <QFileDialog>
#include <QErrorMessage>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QTableWidgetItem>
#include "company.h"
#include "element.h"
#include "tablemodel.h"

namespace Ui {
class MainWindow;
class AddProduct;
class CreateProduct;
class DeleteProduct;
class LoadBuyer;
class FileDialog;
}

inline void swap(QJsonValueRef v1, QJsonValueRef v2) {
    QJsonValue tmp(v1);
    v1 = v2;
    v2 = tmp;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void loadFromJSON();
    void saveToJSON();
    void sumTotal() const;

    Ui::MainWindow *ui;
    QString mDestFile;
    QString mDestUrl;
    QComboBox* mComboCompany;
    QPushButton* mBtnSaveBuyer;
    QPushButton* mBtnLoadBuyer;
    QPushButton* mBtnDestination;
    QPushButton* mBtnGenerate;
    QPushButton* mBtnAddProduct;
    QPushButton* mBtnRemoveProduct;
    QPushButton* mBtnCreateProduct;
    QPushButton* mBtnDeleteProduct;
    QVector<Company> mBuyers;
    QJsonDocument mDoc;
    TableModel* mModel;

private slots:
    void saveBuyer();
    void loadBuyer();
    void addProduct();
    void removeProduct();
    void createProduct();
    void deleteProduct();
    void getDestination();
    void runGenerator();

};

#endif // MAINWINDOW_H
