#!/usr/bin/env python
import os
import sys
from pylatex import Document, Command
from pylatex.base_classes import Environment
from pylatex.utils import NoEscape
import argparse
import json

class Invoice(Environment):
    _latex_name = 'invoice'
    escape = False
    content_separator = "\n"

class InvoiceTable(Environment):
    _latex_name = 'invoicetable'
    escape = False
    content_separator = "\n"

parser = argparse.ArgumentParser()
parser.add_argument("--input", dest="inputFile", help="Source file.", required=True)
parser.add_argument("--output", dest="destFile", help="Destination file.", required=True)
args = parser.parse_args()

includePath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')
os.environ["TEXINPUTS"] = ".;%s;" % includePath

with open(args.inputFile, encoding='utf-8') as f:
    data = json.load(f)

serial = data['Broj racuna']
date = data['Datum']
buyer = data['Kupac']
company = data['Prodavac']
elements = data['Proizvodi']
isEstimate = data['Predracun']

doc = Document()
doc.preamble.append(Command('title', 'Faktura'))
doc.preamble.append(Command('input', 'structure.tex'))

if isEstimate:
    doc.preamble.append(Command('tabletitle', 'PREDRAČUN'))
    doc.preamble.append(Command('serialname', 'predračuna'))
else:
    doc.preamble.append(Command('tabletitle', 'RAČUN-OTPREMNICA'))
    doc.preamble.append(Command('serialname', 'računa-otpremnice'))



doc.preamble.append(Command('invoiceref', serial))
doc.preamble.append(Command('taxrate', '0'))
doc.preamble.append(Command('invoiceissued', date))
doc.preamble.append(Command('invoicedue', NoEscape(r'\mydate\DayAfter[30]')))

# Buyer info
doc.preamble.append(Command('payeename', buyer['Ime']))
doc.preamble.append(Command('payeePIB', buyer['PIB']))
doc.preamble.append(Command('payeeaddress', buyer['Adresa']))
doc.preamble.append(Command('payeeaddresscity', buyer['Grad']))
doc.preamble.append(Command('payeeMB', buyer['Maticni broj']))

# Seller info
doc.preamble.append(Command('companyname', company['Ime']))
doc.preamble.append(Command('sendername', company['Vlasnik']))
doc.preamble.append(Command('senderPIB', company['PIB']))
doc.preamble.append(Command('senderaddress', company['Adresa']))
doc.preamble.append(Command('senderaddresscity', company['Grad']))
doc.preamble.append(Command('sendercontact', company['Kontakt']))
doc.preamble.append(Command('senderaccount', company['TR']))
doc.preamble.append(Command('senderMB', company['Maticni broj']))


doc.append(Command('SetDate', options=date.replace('.', '/')))

with doc.create(Invoice()):
    with doc.create(InvoiceTable()):
        i = 0;
        for element in elements:
            i += 1
            doc.append(Command('invoiceitem', arguments=[i, element['Ime'], 'KOM', element['Kolicina'], element['Cena']]))


outputPath = args.destFile.rsplit('.', 1)[0]

#doc.generate_tex(outputPath)
# Use pdflatex compiler and avoid usage of Perl.
doc.generate_pdf(outputPath, compiler='pdflatex')

sys.exit(0)
