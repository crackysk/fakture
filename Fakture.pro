#-------------------------------------------------
#
# Project created by QtCreator 2018-12-25T00:26:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Fakture
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11
CONFIG -= debug_and_release debug_and_release_target

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    company.cpp \
    element.cpp \
    tablemodel.cpp

HEADERS += \
        mainwindow.h \
    globals.h \
    company.h \
    element.h \
    tablemodel.h

FORMS += \
        mainwindow.ui \
    addproduct.ui \
    createproduct.ui \
    deleteproduct.ui \
    loadbuyer.ui \
    filedialog.ui

# Copy data  to out dir.
copydata.commands = $(COPY_FILE) $$shell_path($$PWD/generator.py) $$shell_path($$OUT_PWD) $$escape_expand(\n\t)
copydata.commands += $(COPY_DIR) $$shell_path($$PWD/data) $$shell_path($$OUT_PWD/data) $$escape_expand(\n\t)
copydata.commands += $(COPY_DIR) $$shell_path($$PWD/third_party) $$shell_path($$OUT_PWD/third_party)
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    generator.py \
    data/structure.tex \
    data/data.json
