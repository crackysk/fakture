#include "company.h"

Company::Company(QString &&name, QString &&director, QString &&PIB, QString &&address, QString &&city, QString &&MB, QString &&contact, QString &&account)
    : mName(name), mDirector(director), mPIB(PIB), mAddress(address), mCity(city), mMB(MB), mContact(contact), mAccount(account)
{

}

QJsonObject Company::toJSON() const
{
    QJsonObject obj;
    obj.insert("Ime", mName);
    obj.insert("Vlasnik", mDirector);
    obj.insert("PIB", mPIB);
    obj.insert("Adresa", mAddress);
    obj.insert("Grad", mCity);
    obj.insert("Maticni broj", mMB);
    obj.insert("Kontakt", mContact);
    obj.insert("TR", mAccount);

    return obj;
}

QString Company::name() const
{
    return mName;
}

QString Company::director() const
{
    return mDirector;
}

QString Company::PIB() const
{
    return mPIB;
}

QString Company::address() const
{
    return mAddress;
}

QString Company::city() const
{
    return mCity;
}

QString Company::MB() const
{
    return mMB;
}

QString Company::contact() const
{
    return mContact;
}

QString Company::account() const
{
    return mAccount;
}
