#ifndef GLOBALS_H
#define GLOBALS_H

#include <QStringList>
#include <QMap>
#include "company.h"

namespace globals {

    static Company company1 ("BOKI-EDI", "Snežana Karasek", "104918713", "Janka Cmelika 13", "11272 Beograd", "60610169", "bokiedi@gmail.com", " 205-117277-76");
    static Company company2 ("SZR \"MB METALPRES\"", "Bećir Murtezani", "109555980", "Jovana Stojsavljevića 1/20", "11080 Beograd", "64261355", "060/33-31-408", "205-233685-52");

    // Map of companies with their PIB's.
    static QMap<QString, Company> companies {{company1.name(), company1}, {company2.name(), company2}};
};

#endif // GLOBALS_H
