#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_addproduct.h"
#include "ui_createproduct.h"
#include "ui_deleteproduct.h"
#include "ui_loadbuyer.h"
#include "ui_filedialog.h"
#include "globals.h"

#include <QFileSystemModel>
#include <QFileIconProvider>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mDestFile()
{
    ui->setupUi(this);
    mModel = new TableModel(this);

    loadFromJSON();

    // Initialization step.
    mComboCompany = ui->comboCompany;
    mBtnSaveBuyer = ui->btnSaveBuyer;
    mBtnLoadBuyer = ui->btnLoadBuyer;
    mBtnAddProduct = ui->btnAddProduct;
    mBtnRemoveProduct = ui->btnRemoveProduct;
    mBtnCreateProduct = ui->btnCreateProduct;
    mBtnDeleteProduct = ui->btnDeleteProduct;
    mBtnDestination = ui->btnDest;
    mBtnGenerate = ui->btnGenerate;


    mModel->setHeaderData(0, Qt::Horizontal, Qt::UserRole);
    mModel->setHeaderData(1, Qt::Horizontal, Qt::UserRole);
    mModel->setHeaderData(2, Qt::Horizontal, Qt::UserRole);
    ui->tvContent->setModel(mModel);

    // Set current date as starting point.
    ui->deDate->setDate(QDate::currentDate());

    // Set starting point for destination.
    QJsonObject obj = mDoc.object();
    QJsonValue val = obj.value("destinacija");
    mDestUrl = val.toString();

    // Setup environment.
    mComboCompany->addItems(globals::companies.keys());

    // Connect events with handlers.

    connect(mBtnSaveBuyer, SIGNAL(clicked()), this, SLOT(saveBuyer()));
    connect(mBtnLoadBuyer, SIGNAL(clicked()), this, SLOT(loadBuyer()));
    connect(mBtnAddProduct, SIGNAL(clicked()), this, SLOT(addProduct()));
    connect(mBtnRemoveProduct, SIGNAL(clicked()), this, SLOT(removeProduct()));
    connect(mBtnCreateProduct, SIGNAL(clicked()), this, SLOT(createProduct()));
    connect(mBtnDeleteProduct, SIGNAL(clicked()), this, SLOT(deleteProduct()));
    connect(mBtnDestination, SIGNAL(clicked()), this, SLOT(getDestination()));
    connect(mBtnGenerate, SIGNAL(clicked()), this, SLOT(runGenerator()));

}

void MainWindow::saveBuyer()
{
    QString companyName = ui->leCompanyName->text();
    QString PIB = ui->lePIB->text();
    QString address = ui->leAddress->text();
    QString city = ui->leCity->text();
    QString MB = ui->leMB->text();

    QJsonObject obj = mDoc.object();
    QJsonValue val = obj.value("kupci");

    if (val.isArray()) {
        QJsonArray arr = val.toArray();
        QJsonObject company;
        company.insert("Ime", QJsonValue::fromVariant(companyName));
        company.insert("PIB", QJsonValue::fromVariant(PIB));
        company.insert("Adresa", QJsonValue::fromVariant(address));
        company.insert("Grad", QJsonValue::fromVariant(city));
        company.insert("Maticni broj", QJsonValue::fromVariant(MB));
        arr.append(company);

        obj.insert("kupci", arr);
        mDoc.setObject(obj);
        saveToJSON();

        QMessageBox msg(this);
        msg.setText("Kupac uspešno sačuvan.");
        msg.exec();
    } else {
        qDebug() << "Error with JSON.";
        QApplication::quit();
    }
}

void MainWindow::loadBuyer()
{
    QDialog *dialog = new QDialog(this);
    Ui::LoadBuyer lb;
    lb.setupUi(dialog);

    QJsonObject obj = mDoc.object();
    QJsonValue val = obj.value("kupci");

    if (val.isArray()) {
        QJsonArray arr = val.toArray();
        QStringList companyNames;

        for (QJsonValue element : arr) {
            if (!element.isObject()) {
                qDebug() << "Error with JSON.";
                QApplication::quit();
            }

            QJsonObject company = element.toObject();

            companyNames.append(company.value("Ime").toString());
        }

        lb.cbBuyers->addItems(companyNames);
    } else {
        qDebug() << "Error with JSON.";
        QApplication::quit();
    }

    int retCode = dialog->exec();

    if (retCode) {
        QString companyName = lb.cbBuyers->currentText();

        QJsonArray arr = val.toArray();

        for (QJsonValue element : arr) {
            if (!element.isObject()) {
                qDebug() << "Error with JSON.";
                QApplication::quit();
            }

            QJsonObject company = element.toObject();

            if (company.value("Ime").toString() == companyName) {
                ui->leCompanyName->setText(companyName);
                ui->lePIB->setText(company.value("PIB").toString());
                ui->leAddress->setText(company.value("Adresa").toString());
                ui->leCity->setText(company.value("Grad").toString());
                ui->leMB->setText(company.value("Maticni broj").toString());
                break;
            }
        }
    }

    delete dialog;
}

void MainWindow::addProduct()
{
    QDialog *dialog = new QDialog(this);
    Ui::AddProduct ap;
    ap.setupUi(dialog);

    QJsonObject obj = mDoc.object();
    QJsonValue val = obj.value("proizvodi");

    if (val.isArray()) {
        QJsonArray array = val.toArray();
        QStringList products;

        for (QJsonValue product : array) {
            products.push_back(product.toString());
        }

        ap.cbProducts->addItems(products);
    } else {
        qDebug() << "Error with JSON.";
        QApplication::quit();
    }

    int retCode = dialog->exec();

    if (retCode) {
        if (ap.leAmount->text().isEmpty() || ap.lePrice->text().isEmpty()) {
            QErrorMessage msg(this);
            msg.setMinimumSize(200, 100);
            msg.showMessage("Polja 'količina' i 'cena' su obavezni!");
            msg.exec();
            delete dialog;
            return;
        }
        mModel->insertRows(0, 1);

        QModelIndex index = mModel->index(0, 0);
        mModel->setData(index, ap.cbProducts->currentText());
        index = mModel->index(0, 1);
        mModel->setData(index, ap.leAmount->text().toInt());
        index = mModel->index(0, 2);
        mModel->setData(index, ap.lePrice->text().toDouble());
    }

    sumTotal();

    delete dialog;
}

void MainWindow::removeProduct()
{
    QModelIndexList indeces = ui->tvContent->selectionModel()->selectedRows();
    // Sort in ascending order, since QModelIndex does not have greater operator implemented.
    std::sort(indeces.begin(), indeces.end(), std::less<QModelIndex>());

    // Iterate list in reverse order, so we get descending order.
    for(auto index = indeces.rbegin(); index != indeces.rend(); index++) {
        mModel->removeRows(index->row(), 1, *index);
    }

    sumTotal();
}

void MainWindow::createProduct()
{
    QDialog *dialog = new QDialog(this);
    Ui::CreateProduct cp;
    cp.setupUi(dialog);

    int retCode = dialog->exec();

    if (retCode) {
        QString productName = cp.leProductName->text();
        if (productName.isEmpty()) {
            QErrorMessage msg(this);
            msg.setMinimumSize(200, 100);
            msg.showMessage("Ime proizvoda nije validno!");
            msg.exec();
            delete dialog;
            return;
        }

        QJsonObject obj = mDoc.object();
        QJsonValue val = obj.value("proizvodi");

        if (val.isArray()) {
            QJsonArray arr = val.toArray();
            arr.append(QJsonValue(productName));
            obj.insert("proizvodi", arr);
            mDoc.setObject(obj);
            saveToJSON();
        } else {
            qDebug() << "Problem with JSON.";
            QApplication::quit();
        }
    }

    delete dialog;
}

void MainWindow::deleteProduct()
{
    QDialog *dialog = new QDialog(this);
    Ui::DeleteProduct dp;
    dp.setupUi(dialog);

    QJsonObject obj = mDoc.object();
    QJsonValue val = obj.value("proizvodi");

    if (val.isArray()) {
        QJsonArray array = val.toArray();
        QStringList products;

        for (QJsonValue product : array) {
            products.push_back(product.toString());
        }

        dp.cbProducts->addItems(products);
    } else {
        qDebug() << "Error with JSON.";
        QApplication::quit();
    }

    int retCode = dialog->exec();

    if (retCode) {
        int index = dp.cbProducts->currentIndex();

        QJsonArray arr = val.toArray();
        arr.removeAt(index);
        obj.insert("proizvodi", arr);
        mDoc.setObject(obj);
        saveToJSON();
    }


    delete dialog;
}

void MainWindow::getDestination()
{
    QDialog* dialog = new QDialog(this);
    Ui::FileDialog fd;
    fd.setupUi(dialog);

    // Made custom FileIconProvider to avoid Windows 10 update issue encountered in February 2023.
    class CustomFileIconProvider : public QFileIconProvider {
    public:
        QIcon icon(const QFileInfo &info) const override {
            Q_UNUSED(info);
            return QIcon();
        }
    };

    // Setting default proportions of tree and list view model.
    fd.splitter->setStretchFactor(0, 1);
    fd.splitter->setStretchFactor(1, 2);

    // Setting source for tree view
    QFileSystemModel* dirModel = new QFileSystemModel(this);

    // Filtering
    dirModel->setFilter(QDir::NoDotAndDotDot | QDir::AllDirs);

    dirModel->setRootPath(mDestUrl);
    dirModel->setIconProvider(new CustomFileIconProvider());
    fd.treeView->setModel(dirModel);
    for (int i = 1; i < fd.treeView->header()->length(); i++) {
        fd.treeView->hideColumn(i);
    }

    // Setting source for list view
    QFileSystemModel* fileModel = new QFileSystemModel(this);

    // Filtering
    QStringList filters;
    filters << "*.pdf";
    fileModel->setNameFilters(filters);
    fileModel->setNameFilterDisables(false);
    fileModel->setFilter(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files);

    fileModel->setRootPath(mDestUrl);
    fileModel->setIconProvider(new CustomFileIconProvider());
    fd.tableView->setModel(fileModel);
    fd.tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    // connects
    connect(fd.treeView, &QTreeView::clicked, this, [&](const QModelIndex& index) {
        // Set path of tableView according to selected folder
        QString path = dirModel->fileInfo(index).absoluteFilePath();
        fd.tableView->setRootIndex(fileModel->setRootPath(path));
    });
    connect(fd.tableView, &QTableView::doubleClicked, this, [&](const QModelIndex& index) {
        QString path = fileModel->fileInfo(index).absoluteFilePath();
        fd.tableView->setRootIndex(fileModel->setRootPath(path));
        QModelIndex dirModelIndex = dirModel->setRootPath(path);
        fd.treeView->collapseAll();
        fd.treeView->setCurrentIndex(dirModelIndex);
        fd.treeView->expand(dirModelIndex);
    });
    connect(fd.tableView, &QTableView::clicked, this, [&](const QModelIndex& index) {
       QString fileName = fileModel->fileInfo(index).baseName();
       if (fileModel->fileInfo(index).isFile()) {
           fd.fileName->setText(fileName);
       }
    });

    // Disconnect all connections of this button, in order to prevent from closing.
    fd.buttonBox->button(QDialogButtonBox::Save)->disconnect();
    connect(fd.buttonBox->button(QDialogButtonBox::Save), &QPushButton::clicked, this, [&]() {
        mDestFile = QDir(fileModel->rootPath()).filePath(fd.fileName->text().trimmed() + fd.comboBox->currentData().toString());
        if (QFile(mDestFile).exists()) {
            QMessageBox msg(this);
            msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msg.setDefaultButton(QMessageBox::No);
            msg.setText("File already exists. Are you sure you want to overwrite?");
            int retCode = msg.exec();

            // if user doesn't want to overwrite, return to starting point.
            if (retCode == QMessageBox::No) {
                mDestFile = "";
                return;
            }
        }
        dialog->accept();
    });

    // combo box for types
    fd.comboBox->addItem("PDF file (*.pdf)", QVariant(".pdf"));

    int retCode = dialog->exec();

    if (!retCode || fd.fileName->text().trimmed().isEmpty()) {
        mDestFile = "";
        QErrorMessage msg(this);
        msg.setMinimumSize(200, 100);
        msg.showMessage("Problem pri izboru destinacije!");
        msg.exec();
    }

    delete dialog;
}

void MainWindow::runGenerator()
{
    if (mDestFile.isEmpty()) {
        QErrorMessage msg(this);
        msg.setMinimumSize(200, 100);
        msg.showMessage("Prvo izaberite destinaciju izlaznog pdf fajla!");
        msg.exec();
        return;
    }

    QString companyName = mComboCompany->currentText();

    Company company = *globals::companies.find(companyName);
    Q_UNUSED(company);

    QDate date = ui->deDate->date();
    bool isEstimate = ui->cbEstimate->isChecked();
    QString serial = ui->leSerial->text();

    if (serial.isEmpty()) {
        QErrorMessage msg(this);
        msg.setMinimumSize(200, 100);
        msg.showMessage("Broj računa mora biti naveden!");
        msg.exec();
        return;
    }

    QString buyerName = ui->leCompanyName->text();
    QString buyerPIB = ui->lePIB->text();
    QString buyerAddress = ui->leAddress->text();
    QString buyerCity = ui->leCity->text();
    QString buyerMB = ui->leMB->text();

    Company buyer (std::move(buyerName), "", std::move(buyerPIB), std::move(buyerAddress), std::move(buyerCity), std::move(buyerMB));

    QList<Element> elements = mModel->elements();

    QJsonObject companyInfo = company.toJSON();
    QJsonObject buyerInfo = buyer.toJSON();

    QJsonArray elementsInfo;
    for (Element element : elements) {
        elementsInfo.append(element.toJSON());
    }

    QJsonObject obj;
    obj.insert("Prodavac", companyInfo);
    obj.insert("Kupac", buyerInfo);
    obj.insert("Broj racuna", QJsonValue::fromVariant(serial));
    obj.insert("Datum", QJsonValue::fromVariant(date.toString("dd.MM.yyyy")));
    obj.insert("Predracun", QJsonValue::fromVariant(isEstimate));
    obj.insert("Proizvodi", elementsInfo);

    QJsonDocument info;
    info.setObject(obj);

    // Will be removed automatically at the end of scope.
    QTemporaryFile infoFile;
    if (!infoFile.open()) {
        QErrorMessage msg(this);
        msg.showMessage("Greska pri kreiranju fajla " + infoFile.fileName() + "   sa argumentima!");
        msg.exec();
    }

    infoFile.write(info.toJson());
    infoFile.close();

    QStringList params {"generator.py", "--input", infoFile.fileName(), "--output", mDestFile};

    QProcess generator(this);
    generator.start("python", params);
    generator.waitForFinished();
    if (generator.exitCode()) {
        QErrorMessage msg(this);
        msg.showMessage("Greska pri generisanju pdf-a! Proverite da li je pdf zatvoren. " + generator.readAll());
        msg.exec();
    } else {
        QMessageBox msg(this);
        msg.setText("pdf uspešno izgenerisan.");
        msg.exec();
    }
}

void MainWindow::loadFromJSON()
{
    // Load JSON from file.
    QString data;
    QFile dataFile("data/data.json");

    dataFile.open(QIODevice::ReadOnly | QIODevice::Text);
    data = dataFile.readAll();
    dataFile.close();

    mDoc = QJsonDocument::fromJson(data.toUtf8());
}

void MainWindow::saveToJSON()
{
    QFile dataFile("data/data.json");

    QJsonObject obj = mDoc.object();

    QJsonValue products = obj.value("proizvodi");
    Q_ASSERT(products.isArray());
    QJsonArray productsArray = products.toArray();
    std::sort(productsArray.begin(), productsArray.end(), [](const QJsonValue& lhs, const QJsonValue& rhs) {
       return QString::compare(lhs.toString(), rhs.toString()) <= 0;
    });
    obj.insert("proizvodi", productsArray);

    QJsonValue buyers = obj.value("kupci");
    Q_ASSERT(buyers.isArray());
    QJsonArray buyersArray = buyers.toArray();
    std::sort(buyersArray.begin(), buyersArray.end(), [](const QJsonValue& lhs, const QJsonValue& rhs) {
        QString lhsName = lhs.toObject().value("Ime").toString();
        QString rhsName = rhs.toObject().value("Ime").toString();
        return QString::compare(lhsName, rhsName) <= 0;
    });

    obj.insert("kupci", buyersArray);

    mDoc.setObject(obj);

    dataFile.open(QIODevice::WriteOnly | QIODevice::Text);
    dataFile.write(mDoc.toJson());

    dataFile.close();
}

void MainWindow::sumTotal() const
{
    QList<Element> elements = mModel->elements();

    double sum = 0;
    for (Element element : elements) {
        sum += (element.amount()* element.price());
    }

    ui->leTotal->setText(QString::number(sum));
}

MainWindow::~MainWindow()
{
    delete ui;
}
