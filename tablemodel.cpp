#include "tablemodel.h"

TableModel::TableModel(QObject* parent)
    : QAbstractTableModel(parent)
{

}

TableModel::TableModel(const QList<Element> &&elements, QObject* parent)
    : QAbstractTableModel(parent),
      mElements(elements)
{

}


int TableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(mElements.size());
}

int TableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 3;
}

QVariant TableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= mElements.size() || index.row() < 0) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        const auto &element = mElements.at(index.row());

        switch (index.column()) {
            case 0:
                return element.name();
            case 1:
                return element.amount();
            case 2:
                return element.price();
        }
    }

    return QVariant();
}


QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
            case 0:
                return tr("Ime proizvoda");
            case 1:
                return tr("Količina");
            case 2:
                return tr("Cena");
            default:
                return QVariant();
        }
    }
    return QVariant();
}

bool TableModel::insertRows(int position, int rows, const QModelIndex &index)
{
    Q_UNUSED(index);
    beginInsertRows(QModelIndex(), position, position + rows - 1);

    for (int row = 0; row < rows; ++row)
        mElements.insert(position, Element(QString(), int(), double()));

    endInsertRows();
    return true;
}


bool TableModel::removeRows(int position, int rows, const QModelIndex &index)
{
    Q_UNUSED(index);
    beginRemoveRows(QModelIndex(), position, position + rows - 1);

    for (int row = 0; row < rows; ++row)
        mElements.removeAt(position);

    endRemoveRows();
    return true;
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
        int row = index.row();

        auto element = mElements.value(row);

        switch (index.column()) {
            case 0:
                element.setName(value.toString());
                break;
            case 1:
                element.setAmount(value.toInt());
                break;
            case 2:
                element.setPrice(value.toDouble());
                break;
            default:
                return false;

        }

        mElements.replace(row, element);
        emit(dataChanged(index, index));

        return true;
    }

    return false;
}

Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

QList<Element> TableModel::elements() const
{
    return mElements;
}
