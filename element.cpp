#include "element.h"

Element::Element(const QString &&name, int amount, double price)
    : mName(name), mAmount(amount), mPrice(price)
{

}

QJsonObject Element::toJSON() const {
    QJsonObject obj;
    obj.insert("Ime", mName);
    obj.insert("Kolicina", mAmount);
    obj.insert("Cena", mPrice);

    return obj;
}

QString Element::name() const
{
    return mName;
}

int Element::amount() const
{
    return mAmount;
}

double Element::price() const
{
    return mPrice;
}

void Element::setName(const QString &name)
{
    mName = name;
}

void Element::setAmount(int amount)
{
    mAmount = amount;
}

void Element::setPrice(double price)
{
    mPrice = price;
}
