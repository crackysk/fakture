#ifndef ELEMENT_H
#define ELEMENT_H

#include <QString>
#include <QJsonObject>

class Element
{
public:
    Element() =default;
    Element(const QString &&name, int amount, double price);

    QJsonObject toJSON() const;

    QString name() const;
    int amount() const;
    double price() const;

    void setName(const QString &name);
    void setAmount(int amount);
    void setPrice(double price);

private:
    QString mName;
    int mAmount;
    double mPrice;
};

#endif // ELEMENT_H
